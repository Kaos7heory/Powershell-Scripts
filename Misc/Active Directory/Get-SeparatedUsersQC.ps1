﻿# Edit Path and filename of TempFile and Report Here:
$tempfile = "C:\temp\SeparatedUsersTempFile.csv"
$outputfile = "C:\temp\SeparatedUsersReport.csv"


#Get list of all users in Separated Employees OU and save to a temp file
Get-ADUser -Filter * -SearchBase "OU=Separated Employees,DC=NACR,DC=COM" -Properties SamAccountName | Select SamAccountName | Export-csv $tempfile -NoTypeInformation

#Import the list of users from Temp File
$Users = Import-Csv -Path $tempfile

#Collect Properites of Users
$Data = foreach ($user in $users) {

$Name = Get-aduser $user.SamAccountName -Properties DisplayName,Enabled,emailaddress,msRTCSIP-PrimaryUserAddress,Description,Manager | select DisplayName,Enabled,emailaddress,msRTCSIP-PrimaryUserAddress,Description,Manager
$groups = (Get-ADUser $user.SamAccountName -Properties MemberOf | Select -ExpandProperty memberof).count
$Email = if ($Name.emailaddress -eq $null) {"No"} else {"Yes"}
$Lync = if ($Name.'msRTCSIP-PrimaryUserAddress' -eq $null)  {"No"} else {"Yes"}
$Manager = if ($Name.Manager -eq $null)  {"No"} else {"Yes"}
$DirectReports = (Get-ADUser $user.SamAccountName -Properties DirectReports | Select -ExpandProperty DirectReports).count

#Format the properties into a table
$Props = @{
Name = $Name.DisplayName
SamAccountName = $User.SamAccountName
Groups = $Groups
Enabled = $Name.Enabled
Mail = $Email
Lync = $Lync
Description = $Name.Description
Manager = $Manager
DirectReports = $DirectReports
}

New-Object -TypeName PSObject -Property $Props
}

#Export Results to CSV
$Data | Select Name,SamAccountName,Description,Groups,Enabled,Mail,Lync,Manager,DirectReports | Export-Csv $outputfile -NoTypeInformation