﻿####################################################
#
# This one gets all your servers in AD and dumps the drives with sizes and remaining free space to a CSV (drivespace.csv). 
# It also export some other files - pingable.txt, pingfail.txt, and servers.csv. Those should be self-explanatory.
#
####################################################
$RootPath = Read-Host "Enter export directory (with trailing slash)"
 
Import-Module ActiveDirectory
Write-Output "Getting servers from AD..."
$ADServers = Get-ADComputer -Filter {operatingsystem -like "Windows Server*"} | Where {$_.Enabled -eq "True"} | select Name | sort Name
$ADServers | ConvertTo-CSV -NoTypeInformation | Select -Skip 1 | Out-File "$RootPath\Servers.csv"
 
$Output = @()
$Pingable = @()
$PingFail = @()
$Iteration = 0
$Count = $ADServers.Count
 
cls
"`n`n`n`n`n`n`n`n"
 
ForEach ($Server in $ADServers){
    #Show progress
    $PercentComplete = [math]::Round((($Iteration / $Count) * 100),0)
    If ($PercentComplete -lt 100){Write-Progress -Activity ("Getting disk info from " + $Server.Name + "..") -Status "$PercentComplete% Complete ($Iteration/$Count)" -PercentComplete $PercentComplete}
    Else {Write-Progress -Activity ("Getting disk info from" + $Server.Name + "..") -Status "$PercentComplete% Complete ($Iteration/$Count)" -PercentComplete $PercentComplete -Completed}
 
    #Tests ping. Only tries a second time if first ping fails.
    If (Test-Connection -ComputerName $Server.Name -Count 1 -ErrorAction SilentlyContinue){$Ping = $True}
    ElseIf (Test-Connection -ComputerName $Server.Name -Count 1 -ErrorAction SilentlyContinue){$Ping = $True}
    Else {$Ping = $False}
 
    If ($Ping){
 
        $Pingable += $Server.Name
 
        $DiskInfo = Get-WMiObject -ComputerName $Server.Name win32_logicaldisk -Filter "drivetype=3" -ErrorAction SilentlyContinue
 
        ForEach ($Disk in $DiskInfo){
            $Size = [math]::Round(($Disk.Size/1gb),2)
            $FreeSpace = [math]::Round(($Disk.FreeSpace/1gb),2)
            $PercentFree = [math]::Round((($Disk.FreeSpace * 100.0)/$Disk.Size),2)
 
            $Obj = New-Object -TypeName PSObject
            $Obj | Add-Member -MemberType NoteProperty -Name "SystemName" -Value $Disk.SystemName
            $Obj | Add-Member -MemberType NoteProperty -Name "DeviceID" -Value $Disk.DeviceID
            $Obj | Add-Member -MemberType NoteProperty -Name "Size" -Value $Size
            $Obj | Add-Member -MemberType NoteProperty -Name "FreeSpace" -Value $FreeSpace
            $Obj | Add-Member -MemberType NoteProperty -Name "PercentFree" -Value $PercentFree
            $Obj | Add-Member -MemberType NoteProperty -Name "Label" -Value $Disk.Volumename
 
            $Output += $Obj
        }
    }
    Else {Write-Warning ("Ping failed for " + $Server.Name + ".")}
    $Iteration++
}
 
$Pingable | Out-File "$RootPath\Pingable.txt"
$PingFail | Out-File "$RootPath\PingFail.txt"
$Output | Sort SystemName,Drive | Export-CSV "$RootPath\drivespace.csv" -NoTypeInformation