﻿function Set-Reg {
    param (
        [string]$key,
        [string]$name,
        [string]$value,
        [string]$type
    )
 
    If((Test-Path -Path $key) -eq $false) {
        New-Item -Path $key
    }
    $k = Get-Item -Path $key
    If($k.GetValue($name) -eq $null) {
        New-ItemProperty -Path $key -Name $name -Value $value -PropertyType $type
    } else {
        Set-ItemProperty -Path $key -Name $name -Value $value
    }
}
 
#Set execution policy
Set-ExecutionPolicy bypass -Scope CurrentUser -Force -Confirm:$false
 
#Set Windows Explorer settings
$key = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced\'
Set-Reg -key $key -name 'HideFileExt' -value 0 -type 'DWord'
Set-Reg -key $key -name 'NavPaneShowAllFolders' -value 1 -type 'DWord'
Set-Reg -key $key -name 'NavPaneExpandToCurrentFolder' -value 1 -type 'DWord'
Set-Reg -key $key -name 'Start_NotifyNewApps' -value 0 -type 'DWord'
Set-Reg -key $key -name 'Start_ShowMyComputer' -value 2 -type 'DWord'
Set-Reg -key $key -name 'Start_ShowControlPanel' -value 2 -type 'DWord'
Set-Reg -key $key -name 'Start_ShowRun' -value 1 -type 'DWord'
Set-Reg -key $key -name 'StartMenuAdminTools' -value 2 -type 'DWord'
Set-Reg -key $key -name 'DontUsePowerShellOnWinx' -value 0 -type 'DWord'
 
#Set cmd settings
$key = 'HKCU:\Console\'
Set-Reg -key $key -name 'QuickEdit' -value 1 -type 'DWord'
Set-Reg -key $key -name 'ScreenBufferSize' -value 196608120 -type 'DWord'
Set-Reg -key $key -name 'WindowSize' -value 2621560 -type 'DWord'
 
#Set IE defaults
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'start page' -value 'http://www.google.com' -type 'String'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'IE8TourShown' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'IE8RunOncePerInstallCompleted' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'IE8TourNoShow' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'DisableFirstRunCustomize' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'IE9TourShown' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'IE10TourShown' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'IE11TourShown' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'RunOnceComplete' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'RunOnceHasShown' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'Friendly http errors' -value 'no' -type 'String'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\Main\' -name 'Check_Associations' -value 'no' -type 'String'
Set-Reg -key 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\' -name 'WarnonZoneCrossing' -value 0 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\' -name 'IEHarden' -value 0 -type 'DWord'
#Set-Reg -key 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\' -name 'IEHarden' -value 0 -type 'DWord'
#Set-Reg -key 'HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}\' -name 'IsInstalled' -value 0 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'WarnOnclose' -value 0 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'OpenInForeground' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'Groups' -value 0 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'NewTabNextToCurrent' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'NewTabPageShow' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'ShortcutBehavior' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Internet Explorer\TabbedBrowsing\' -name 'ThumbnailBehavior' -value 0 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\' -name 'ConfirmFileDelete' -value 1 -type 'DWord'
Set-Reg -key 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\StartPage\' -name 'OpenAtLogon' -value 0 -type 'DWord'
 
Stop-Process -processname explorer -confirm:$false -Force
 
#Disable Loopback Check
Set-Reg -key 'HKLM:\System\CurrentControlSet\Control\Lsa' -name 'DisableLoopbackCheck' -value 1 -type 'Dword'
 
#Disable screensaver
Set-Reg -key 'HKCU:\Control Panel\Desktop' -name 'ScreenSaveActive' -value 0 -type 'String'
Set-Reg -key 'HKCU:\Software\Policies\Microsoft\Windows\Control Panel\Desktop' -name 'ScreenSaveActive' -value 0 -type 'String'
 
#Disable UAC on 2012 *Careful this can break DCs* *well it did once, just be careful*
#New-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system" -Name EnableLUA -PropertyType DWord -Value 0 -Force
#Set Print Spooler to manual startup
#Set-Service spooler -StartupType Manual
 
<#Download SysInternals Tools
cd 'c:\Program Files'
md SysInternals
cd SysInternals
$sysinternals =  invoke-webrequest http://live.sysinternals.com/
$sysinternals.links | % {$file = $_.innerHTML; invoke-webrequest http://live.sysinternals.com/$file -outfile .\$file}
#>
 
New-item –type file –force $profile; (Invoke-WebRequest http://mydomain.com/psprofile.txt).Content | Out-File $profile