﻿<#
.SYNOPSIS
  Author:..John
  Date:....2013.12.27
 
.DESCRIPTION
  Specify targetHost.  The script will connect and obtain the asset tag (WMI SerialNumber), and then connect to
  Dell's warranty API and output general system info as well as any warranty information for non-expired warranties.
 
#>
 
$targetHost = "." # target computer
$cutoffDays = 60 # write a warning for each warranty that ends sooner than this number of days
 
#Date/time pattern returned from Dell's API
$pattern = 'yyyy\-MM\-dd\Thh\:mm\:ss'
 
$serialNumber = (Get-WmiObject -class Win32_SystemEnclosure -computername $targetHost -namespace "root\CIMV2").SerialNumber
 
$doc = New-Object System.Xml.XmlDocument
$doc.Load("https://api.dell.com/support/v2/assetinfo/warranty/tags?svctags=$serialNumber&apikey=d676cf6e1e0ceb8fd14e8cb69acd812d")
 
write-host -NoNewLine Information for $targetHost
$doc.GetAssetWarrantyResponse.GetAssetWarrantyResult.Response.DellAsset
 
$doc.GetAssetWarrantyResponse.GetAssetWarrantyResult.Response.DellAsset.Warranties.Warranty | ? {
    [DateTime]::ParseExact($_.EndDate, $pattern, $null) -gt [DateTime]::Today
} | % {
    $_
    $expiration = New-TimeSpan $(Get-Date) $([DateTime]::ParseExact($_.EndDate, $pattern, $null))
    if ($expiration.Days -lt $cutoffDays ) {
        Write-Host "Warning: warranty ends in " -ForegroundColor "yellow" -NoNewLine
        Write-Host $expiration.Days -ForegroundColor "yellow" -NoNewLine
        Write-Host " days" -ForegroundColor "yellow"
    }  
}