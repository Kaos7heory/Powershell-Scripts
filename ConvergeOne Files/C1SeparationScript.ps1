#PowerShell script that automates some steps of the separation process for ConvergeOne's production Active Directory domain. It does not affect AD accounts on TRAIN, DEV, or QA.
#Initially created on 5/17/2019 (ver. 19.05.17) by John Roszak for ConvergeOne

#Current Bugs
##Doesn't display exchangeGUID for the first termed user entered after launching the script until you enter 
##a second user to term.

#Planned Additions

#Wishlist
# Automatically start account migration for users on O365

#Variables/Functions
$UserCredential = Get-Credential
$ExchSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://server.domain.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
##Calling Import-PSSession here so the PST export works later
Import-PSSession $ExchSession -DisableNameChecking -AllowClobber | Out-Null
$Share = "\\Server\FileShare"
$NAMShare = "\\Server\FileShare\NAMs"
$Password = ConvertTo-SecureString -AsPlainText "P@ssw0rd01" -Force
$Date = Get-Date -UFormat %D
$Time = Get-Date -UFormat %I:%M%p
$Choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$Choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$Choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

#Function for the separation process.
function Separation {
    #Variables for If/Else loop
    $Username = Read-Host "Please Enter The Username" | Get-ADUser -Properties MemberOf,TargetAddress,ManagedObjects,Name,title
    $Message1 = Write-Output "The account you have entered belongs to $($Username.Name)."
    $Question = 'Do you wish to proceed?'
    $ComputerAccounts = @()
        foreach ( $usernameObject in $Username.ManagedObjects) {
            if ((Get-ADObject $usernameObject | Select-Object Objectclass).Objectclass -eq "computer") {$ComputerAccounts += $usernameObject}
        }
    $Decision1 = $Host.UI.PromptForChoice($Message1, $Question, $Choices, 1)
    Out-File -FilePath $Share\Reports\$($Username.SamAccountName).csv

    #If/Else loop for confirmation prompt
    if ($Decision1 -eq 0) {
        Write-Output 'Running through Separation process...'}
        else {
        Clear-Variable -Name Username
        Clear-Variable -Name ComputerAccounts
		#Starts back at beginning of function
        Separation
    }
    #Disable AD Account
    Set-ADUser $Username -Enabled $false
    Write-Output "$($Date) / $($Time) / $($Username.Name) - AD account disabled" | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv 

    #Password Change
    Set-ADAccountPassword $Username -NewPassword $Password
    Write-Output "$($Date) / $($Time) / $($Username.Name) - Password changed"  | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv

    #Clear Office, Manager, City and Phone Attributes
    Set-ADUser $Username -Clear physicalDeliveryOfficeName,Manager,telephoneNumber,l
    Write-Output "$($Date) / $($Time) / $($Username.Name) - Office, Phone Number and Manager fields cleared"  | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv

    #Add Separation Date to Description
    Set-ADUser $Username -Description "TERMED: $Date"
    Write-Output "$($Date) / $($Time) / $($Username.Name) - Separation date added to account description"  | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv

    #List & Clear Security Groups
    Get-ADPrincipalGroupMembership -Identity $Username | Where-Object -Property Name -ne -Value 'Domain Users' | Sort-Object | Format-Table -Property Name | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv
    ForEach ($MemberOf in $Username.MemberOf) {
        Remove-ADGroupMember -Identity $MemberOf -Members $Username -Confirm:$false
    }
    Write-Output "$($Date) / $($Time) / $($Username.Name) - Security memberships cleared"  | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv

    #Delete Assigned Computer Account Object
    ForEach ($ComputerAccount in $ComputerAccounts){
    if
        ($ComputerAccount -like "*systems*"){
            Remove-ADObject $ComputerAccount -Confirm:$false
            Write-Output "$($Date) / $($Time) / $($Username.Name) - Computer AD account(s) deleted"  | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv
        }
        else {
        }
    }
    #OU Sorting & On-Prem PST Export
    #TargetAddress attribute is blank for on-prem users, populated for O365 users.
    #If user is a National Account Manager, PST is dumped into a different folder but otherwise functionally the same as line 88.
    if
        ($null -eq $Username.TargetAddress -and $Username.title -ne "National Account Manager" -or "Senior National Account Manager" -or "NAM") {
            Write-Output ("$($Username.Name) is a $($Username.title) and has an on-premises mailbox. Account will be moved to `"Separated Users`" OU, beginning export of mailbox to $($Share)\$($Username.SamAccountName).") | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv
            Move-ADObject -Identity $Username -TargetPath "OU=Separated Employees,DC=domain,DC=com"
            Start-Sleep -Seconds 10
            $Data = New-MailBoxExportRequest -Mailbox $Username.SamAccountName -FilePath $Share\$($Username.SamAccountName).pst
            $Email = Read-Host "Please enter your email address to be notified when the PST export has completed"
            Set-Notification -NotificationEmail @($Email) -Identity $Data.RequestGuid.ToString()
        }
        elseif
            ($Username.title -eq "National Account Manager" -or "Senior National Account Manager" -or "NAM" -and $null -eq $Username.TargetAddress){
                Write-Output ("$($Username.Name) is a ($($Username.title) and has an on-premises mailbox. Account will be moved to `"Separated Users`" OU, beginning export of $($Share)\$($Username.SamAccountName).") | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv
                Move-ADObject -Identity $Username -TargetPath "OU=Separated Employees,DC=domain,DC=com"
                Start-Sleep -Seconds 10
                $Data = New-MailBoxExportRequest -Mailbox $Username.SamAccountName -FilePath $NAMShare\$($Username.SamAccountName).pst
                $Email = Read-Host "Please enter your email address to be notified when the PST export has completed"
                Set-Notification -NotificationEmail @($Email) -Identity $Data.RequestGuid.ToString()
            }
        else {
            Write-Output ("$($Username.Name) is a $($Username.title) and has an Office 365 Mailbox. Account will be moved to `"Separated Users Syncing Online`" OU. Running ExchangeGUID Check...") | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv
            Get-RemoteMailbox $Username.SamAccountName | Select-Object ExchangeGUID | Out-File -Append -FilePath $Share\Reports\$($Username.SamAccountName).csv
            Move-ADObject -Identity $Username -TargetPath "OU=Separated Employees Syncing Online,DC=domain,DC=com"
            Write-Output ('Please perform mailbox migration to on-prem.')
        }

    Clear-Variable -Name Username
    Clear-Variable -Name ComputerAccounts
    }
#Start of Script
Separation

#Do/Until Loop for additional separations
Do {
    $Message2 = Write-Output "Is there another account you wish to separate?"
    $Decision2 = $Host.UI.PromptForChoice($Message2, $Question, $Choices, 1)
    #Prompt for additional users
    if ($Decision2 -eq 0) {
       Separation
      } else {
        Write-Output 'Exiting'
        Remove-PSSession $ExchSession
        exit
      }
    } Until ($Decision2 -eq 1)

#Disconnect Remote Exchange PowerShell Session
Remove-PSSession $ExchSession