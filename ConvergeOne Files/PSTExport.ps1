$UserCredential = Get-Credential
$Share = "\\NACR27\OldPSTs$"
$NAMShare = "\\NACR27\OldPSTs$\NAMs"
$ExchSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://NACR502.nacr.com/PowerShell/ -Authentication Kerberos -Credential $UserCredential
Import-PSSession $ExchSession -DisableNameChecking -AllowClobber | Out-Null
$Choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$Choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$Choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))

function PSTExport {
  $Message  = Write-Output "The account you have entered belongs to $($Username.Name)."
  $Question = 'Are you sure you wish to proceed with this account?'
  $Decision1 = $Host.UI.PromptForChoice($message, $question, $choices, 1)

  if ($Decision1 -eq 0) {
    Write-Output "Checking $($Username.Name)'s Title"
  } else {
    Write-Output 'Script cancelled, exiting.'
    Remove-PSSession $ExchSession
    exit
  }

  if 
    ($Username.title -eq "National Account Manager" -or "Senior National Account Manager" -or "NAM"){
      Write-Output "$($Username.Name) is a $($Username.title). Please enter your email address to be notified when the PST export has completed"
      $Email = Read-Host "Email Address"
      $NAMData = New-MailBoxExportRequest -Mailbox $Username.SamAccountName -FilePath $NAMShare\$($Username.SamAccountName).pst
      Set-Notification -NotificationEmail @($Email) -Identity $NAMData.RequestGuid.ToString()
      Write-Output "Beginning PST export to $($NAMShare)"
    }
    else {
      Write-Output "$($Username.Name) is a $($Username.title). Please enter your email address to be notified when the PST export has completed"
      $Data = New-MailBoxExportRequest -Mailbox $Username.SamAccountName -FilePath $Share\$($Username.SamAccountName).pst
      $Email = Read-Host "Please enter your email address"
      Set-Notification -NotificationEmail @($Email) -Identity $Data.RequestGuid.ToString()
      Write-Output "Beginning PST export to $($Share)"
  }
}

PSTExport

#Do/Until Loop for additional separations
Do {
  $Decision2 = $Host.UI.PromptForChoice($Message2, $Question, $Choices, 1)
  $Message2 = Write-Output "Is there another account you wish to export?"
  #Prompt for additional users
  if ($Decision2 -eq 0) {
    PSTExport
    } else {
      Write-Output 'Exiting'
      Remove-PSSession $ExchSession
      exit
    }
  } Until ($Decision2 -eq 1)